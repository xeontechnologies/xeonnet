# XeonNet #
Your friendly Discord Bot powered by many other services.

### What is this repository for? ###
* Storing the files of XeonNet for community contribution or hosting your own copy.
* 0.0.2 - Indev

### How do I get set up? ###
* XeonNet's setup is fairly quick and simple in most cases. Simple install NodeJS Version 7 or higher; Then run start.sh after setting `config.json`'s values.
* Dependencies can be installed via `npm install ./` in the bot's home directory.

### Contribution guidelines ###
* Documentation of EVERYTHING. Doesnt have to be formal docs. Just comments for other users.
* Test and make sure EVERYTHING works. Do not submit untested code.
* Malicious or Dangerous code WILL BE REJECTED

### Who do I talk to? ###
* Samuel Voeller (Lead Dev)
