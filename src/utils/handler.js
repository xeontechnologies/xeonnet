
// Error Handling Utility
// Original Author: Samuel (xCykrix)
// Contributors: ... ... ...
// Description: Handler and Protection for XeonNet
// Global: No -- Uses "process"

// debug
if(Configuration.debugMode !== false) {
  Bot.on('debug', (post) => {
    if(Configuration.tokenProtect === true && post.split(' ').indexOf(Configuration.token) > -1) { post = post.replace(Configuration.token, "<censored>"); }
    if(post === "Sending heartbeat") { return; } // Reduces 2 Way Spam | VVV Debug Post and Reading for HeartBeat to post Custom Message
    if(post === "Heartbeat acknowledged") { Logger.custom('HEARTBEAT', "Pinged Discord | Connection Active."); return; } else { Logger.custom('DEBUG', post); }
  });
}

// error
Bot.on('error', (err) => {
  if(err !== undefined && err !== null) { Logger.error('Bot Generated Error => ' + err.message); }
});
process.on('error', (err) => {
  if(err !== undefined && err !== null) { Logger.error('NodeJS Generated Error => ' + err.message); }
});

// unhandledException
Bot.on('unhandledException', (exc) => {
  if(exc !== undefined && exc !== null) { Logger.error('Bot Generated Exception => ' + exc.message); }
});
process.on('unhandledException', (exc) => {
  if(exc !== undefined && exc !== null) { Logger.error('NodeJS Generated Exception => ' + exc.message); }
});

// unhandledRejection
Bot.on('unhandledRejection', (rej) => {
  if(rej !== undefined && rej !== null) { Logger.error('Bot Generated Rejection => ' + rej.message); }
});
process.on('unhandledRejection', (rej) => {
  if(rej !== undefined && rej !== null) { Logger.error('NodeJS Generated Rejection => ' + rej.message); }
});

Logger.info('Loaded ErrorHandling Protection.');
