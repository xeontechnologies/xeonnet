
// Restart Utility
// Original Author: Samuel (xCykrix)
// Contributors: ... ... ...
// Description: AutoRestart for XeonNet
// Global: No -- Uses "setTimeout"

// Generates Restart Clock
this.init = (() => {
  if(true) { // !!!!!! TODO - $.config.useAutoRestart
    let time = 10800000; // This is 3 Hours
    let hours = (((time/1000)/60)/60);
    Logger.info("Restarting in " + hours + " hours.");
    setTimeout(reboot, time);
  }
});

// Kills Bot
reboot = (() => {
  //$.bot.destroy(); -- Planned Fix | Must make client first...
  setTimeout(process.exit, 7*1000, 1);
});
