
// Logging Utility
// Original Author: Samuel (xCykrix)
// Contributors: ... ... ...
// Description: Generic Logger Utility for XeonNet
// Global: Yes

print = ((level, message) => { // FIX ALL THE LOGGING; NEW FORMAT
  if(Configuration.tokenProtect === true && message.split(' ').indexOf(Configuration.token) > -1) { message = message.replace(Configuration.token, "<censored>"); }
  if(message === undefined) { message = level; level = "info"; }
  let timestamp = (new Date()).toString().substr(16, 8);
  console.log(`(${timestamp})[${level.toUpperCase()}] ${message}`);
});

setupLogger = () => {

  var logger = {
    info: logInfo,
    warn: logWarn,
    error: logError,
    custom: logCustom
  }

  return logger;
}

logInfo = ((message) => {
  print('INFO', message);
});

logWarn = ((message) => {
  print('WARN', message);
});

logError = ((message) => {
  print('ERROR', message);
});

logCustom = ((level, message) => {
  print(level, message);
});

global.Logger = setupLogger();
