clear
echo "XeonNet Productions... [ Endless Restarter ]";
echo " - Loop: Forever until CTRL+C";

while [ true ]; do
  clear
  echo "-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-";
  node   "./boot.js" --max_old_space_size=2048
  nodejs "./boot.js" --max_old_space_size=2048
  echo "-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-";
  echo "Restarting...";  sleep 3;
done

