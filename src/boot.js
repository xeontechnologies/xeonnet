global.Configuration = require('./config.json'); // Loads Config Object
global.Database = require('./app/database/getUtility'); // Loads DB File
require('./utils/logger'); // Creates global "Logger" variable (in file).
Logger.info("Initializing Utilities...");
global.DiscordJS = require('discord.js'); // Creates Discord Lib Instance
global.Bot = new DiscordJS.Client({fetchAllMembers:true}); // Creates Instance
var AutoRestarter = require('./utils/restarter').init(); // Loads AutoRestart
require('./utils/handler'); // Generates Protection against Errors

//========================================================
// XeonNet Version 3 ClientStrapper
//========================================================

Logger.custom('UPDATING', "Attempted a GIT PULL from Parent Repository.");
Logger.info("Handing NodeJS to XeonNet-v3 ClientStrapper.");

global.Loader = require('./utils/loader');
Loader.loadHandlers();
Loader.loadModules();

Bot.login(Configuration.token).catch((err) => Logger.error("Failed to Login => " + err.message));

Bot.once('ready', () => {
  console.log(' ');
  Logger.info("Client Connected!");
  Logger.info("You are officially good to go. You can leave this screen running or (if on linux command line) type ./launcher detach to disconnect.");
  Logger.info("If you are on windows, you can use CTRL+C to kill the process. You can also (again, linux only) use | ./launcher attach | ./launcher kill | aswell.");
  console.log(' ');
});
