templateObject = (guild) => {
  let obj = {
    "guild": `${guild.name}`,
    "id": `${guild.id}`,
    "admins": [],
    "mods": [],
    "enabled": []
  }; return obj;
}

const ndb = require('node-json-db');

checkInvalid = (dataObject) => {
  try { if(dataObject.getData('/id')) { return true; } else { return false; } }
  catch(err) { return false; }
};

getPath = (objPath, guild) => {
  let database = getDatabase(guild);
  try { let data = database.getData(objPath); return data; }
  catch(err) { return undefined; }
}; this.getPath = getPath;

setPath = (objPath, value, bool, guild) => {
  let database = getDatabase(guild);
  try { let data = database.push(objPath, bool); return data; }
  catch(err) { return undefined; }
}; this.setPath = setPath;

getDatabase = (guild) => {
  let dataObj = new ndb(`./application/database/Storage/guilds/${guild.id}.json`, true, true);
  if(!checkInvalid(dataObj)) { dataObj.push('/', templateObject(guild), true); dataObj.reload(); }
  return dataObj;
};

global.hasPermissions = (message, user, type) => {
  if(type === 'dev') {
    if(Configuration.owners.indexOf(user.id) > -1) { return true; }
    return false;
  }
  if(type === 'owner') {
    if(message.guild.owner.id === user.id) { return true; }
    if(Configuration.owners.indexOf(user.id) > -1) { return true; }
    return false;
  }
  if(type === 'admin') {
    if(message.guild.owner.id === user.id) { return true; }
    if(Configuration.owners.indexOf(user.id) > -1) { return true; }
    if(getDatabase(message.guild).getData("/admins").indexOf(user.id) > -1) { return true; }
    return false;
  }
  if(type === 'mod') {
    if(Configuration.owners.indexOf(user.id) > -1) { return true; }
    if(message.guild.owner.id === user.id) { return true; }
    if(getDatabase(message.guild).getData("/admins").indexOf(user.id) > -1) { return true; }
    if(getDatabase(message.guild).getData("/mods").indexOf(user.id) > -1) { return true; }
    return false;
  }
};
