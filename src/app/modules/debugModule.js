const moduleInfo = {
  name: 'Debugging Module',
  truename: 'debugModule',
  author: 'Samuel (xCykrix)',
  contributors: []
}

runDebugCode = (message, params) => {
  let command = "setgame";
  if(!Configuration.debugMode || !hasPermissions(message, message.author, 'dev')) { return; }

  message.channel.sendMessage('Guild ID: ' + Database.getPath('/id', message.guild));

}

this.commands = [
  { module: moduleInfo.truename, names: ['debug', 'test'], use: runDebugCode }
]
