const moduleInfo = {
  name: 'Dev Utils',
  truename: 'dev-utils',
  author: 'Samuel (xCykrix)',
  contributors: []
}

setGame = (message, params) => {
  let command = "setgame";
  if(!hasPermissions(message, message.author, 'dev')) { return; }
  if(params[0] === undefined) {
    message.channel.sendMessage("Please specify atleast 1 word for my game or NONE for blank.").catch((err) => {
      if(Configuration.debugMode) { print('error', `Module ${moduleInfo.name.toUpperCase()} has thrown error in command ${command.toUpperCase()}: ${err.message}`) }
    }); return; }
  if(params[0] === "NONE") { Bot.user.setGame("").catch((err) => {
      if(Configuration.debugMode) { print('error', `Module ${moduleInfo.name.toUpperCase()} has thrown error in command ${command.toUpperCase()}: ${err.message}`) }
    }); return; }
  Bot.user.setGame(params.join(' ')).catch((err) => {
      if(Configuration.debugMode) { print('error', `Module ${moduleInfo.name.toUpperCase()} has thrown error in command ${command.toUpperCase()}: ${err.message}`) }
    });
  message.channel.sendMessage("Updated my playing status!").catch((err) => {
    if(Configuration.debugMode) { print('error', `Module ${moduleInfo.name.toUpperCase()} has thrown error in command ${command.toUpperCase()}: ${err.message}`) }
  });
}

setStatus = (message, params) => {
  let command = "setgame";
  if(!hasPermissions(message, message.author, 'dev')) { return; }
  if(params[0] === undefined || ['online', 'offline', 'dnd', 'idle'].indexOf(params[0]) === -1) {
    message.channel.sendMessage("Please specify online, idle, dnd, or offline for my status.").catch((err) => {
      if(Configuration.debugMode) { print('error', `Module ${moduleInfo.name.toUpperCase()} has thrown error in command ${command.toUpperCase()}: ${err.message}`) }
    }); return; }
  Bot.user.setStatus(params[0]).catch((err) => {
      if(Configuration.debugMode) { print('error', `Module ${moduleInfo.name.toUpperCase()} has thrown error in command ${command.toUpperCase()}: ${err.message}`) }
    });
  message.channel.sendMessage("Updated my user status!").catch((err) => {
    if(Configuration.debugMode) { print('error', `Module ${moduleInfo.name.toUpperCase()} has thrown error in command ${command.toUpperCase()}: ${err.message}`) }
  });
}

reloadAll = (message, params) => {
  let command = "setgame";
  if(!hasPermissions(message, message.author, 'dev')) { return; }
  Loader.reloadModules();
  message.channel.sendMessage("Reloaded Modules :smile:").catch((err) => {
    if(Configuration.debugMode) { print('error', `Module ${moduleInfo.name.toUpperCase()} has thrown error in command ${command.toUpperCase()}: ${err.message}`) }
  });
}


this.commands = [
  { module: moduleInfo.truename, names: ['setplaying', 'setgame'], use: setGame },
  { module: moduleInfo.truename, names: ['setstatus'], use: setStatus },
  { module: moduleInfo.truename, names: ['reload', 'rl'], use: reloadAll }
]
