templateObject = (guild) => {
  let obj = {
    "guild": guild.name,
    "id": guild.id,
    "admins": [],
    "mods": [],
    "enabled": []
  }; return obj;
}

const ndb = require('node-json-db');

checkInvalid = (dataObject) => {
  try { if(dataObject.getData('/id')) { return true; } else { return false; } }
  catch(err) { return false; }
};

getPath = (objPath, value, database) => {
  try { if(database.getData(objPath)) { return true; } else { return false; } }
  catch(err) { database.push(objPath, value); database.reload(); return database; }
}; this.getPath = getPath

getDatabase = (guild) => {
  let dataObj = new ndb(`./application/database/Storage/guilds/${guild.id}.json`, true, true);
  if(checkInvalid(dataObj)) { dataObj.push('/', templateObject, true); dataObj.reload(); }
  return dataObj;
}; this.getDatabase = getDatabase;

global.hasPermissions = (message, user, type) => {
  if(type === 'dev') {
    if($.config.owners.indexOf(user.id) > -1) { return true; } else { return false; }
  }
  if(type === 'admin') {
    if($.config.owners.indexOf(user.id) > -1) { return true; } else { return false; }
    if(getDatabase(message.guild).getData("/admins").indexOf(user.id) > -1) { return true; } else { return false; }
  }
  if(type === 'mod') {
    if($.config.owners.indexOf(user.id) > -1) { return true; } else { return false; }
    if(getDatabase(message.guild).getData("/admins").indexOf(user.id) > -1) { return true; } else { return false; }
    if(getDatabase(message.guild).getData("/mods").indexOf(user.id) > -1) { return true; } else { return false; }
  }
};
