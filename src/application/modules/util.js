var moduleInfo = {
  name: "Utilities", // Pretified Module Name
  module_exact: 'util', // Must Match File Name
  author: "Samuel J Voeller (xCykrix)", // You...
  contributors: "NONE" // Any Helpers? Full name seperated by ; here.
}

// YOUR CODE SHOULD BE HERE AND DOWN

var lastPing = 100;
runPing = (message, params) => {
  let command = "PING";
  message.channel.sendMessage("[Pong] Detecting...").then(msg => {
    msg.edit(`[Pong] ${msg.createdTimestamp - message.createdTimestamp}ms`).catch((err) => {
      if($.config.debug) { print('error', `Module ${moduleInfo.name.toUpperCase()} has thrown error in command ${command.toUpperCase()}: ${err.message}`) }
    });;
    lastPing = (msg.createdTimestamp - message.createdTimestamp);
  }).catch((err) => {
    if($.config.debug) { print('error', `Module ${moduleInfo.name.toUpperCase()} has thrown error in command ${command.toUpperCase()}: ${err.message}`) }
  });
}

// YOU SHOULD NOT PUT PERSONAL CODE PAST THIS LINE!!!

// Register Command Objects
var newCommands = [
  { module: moduleInfo.module_exact, names: ['ping', 'delay', 'lag'], run: runPing }
];

// Registers Commands; Do not edit this or "newCommands" name.
this.register = (list) => {
  newCommands.forEach(command => { list.push(command); });
  return list;
};
