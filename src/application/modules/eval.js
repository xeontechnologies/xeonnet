var moduleInfo = {
  name: "Evaluations", // Pretified Module Name
  module_exact: 'eval', // Must Match File Name
  author: "Samuel J Voeller (xCykrix)", // You...
  contributors: "NONE" // Any Helpers? Full name seperated by ; here.
}

// YOUR CODE SHOULD BE HERE AND DOWN

runEval = (message, params) => {
  let command = "eval";
  if(!hasPermissions(message, message.author, 'dev')) { return; }
  let reply = ""; try { reply = eval(params.join(' ')); } catch(evalError) {
    message.channel.sendMessage("`Code Eval`\n" + '```js\n' + "Error: " + evalError.message + " ```" ).catch((err) => {
      if($.config.debug) { print('error', `Module ${moduleInfo.name.toUpperCase()} has thrown error in command ${command.toUpperCase()}: ${err.message}`) }
    }); return;
  }
  message.channel.sendMessage('`Code Eval`\n' + '```js\n' + reply + " ```").catch((err) => {
    if($.config.debug) { print('error', `Module ${moduleInfo.name.toUpperCase()} has thrown error in command ${command.toUpperCase()}: ${err.message}`) }
  });
}

// YOU SHOULD NOT PUT PERSONAL CODE PAST THIS LINE!!!

// Register Command Objects
var newCommands = [
  { module: moduleInfo.module_exact, names: ['eval'], run: runEval }
];

// Registers Commands; Do not edit this or "newCommands" names.
this.register = (list) => {
  newCommands.forEach(command => { list.push(command); });
  return list;
};
