var moduleInfo = {
  name: "FunStuff", // Pretified Module Name
  module_exact: 'funstuff', // Must Match File Name
  author: "Samuel J Voeller (xCykrix)", // You...
  contributors: "NONE" // Any Helpers? Full name seperated by ; here.
}

// YOUR CODE SHOULD BE HERE AND DOWN

runRollTheDice = (message, params) => {
  let command = "roll";
  if(params[0] === undefined || parseInt(params[0]) === NaN || parseInt(params[0]) <= 1) {
    message.channel.sendMessage("Please specify the number of sides. [Two or more]").catch((err) => {
      if($.config.debug) { print('error', `Module ${moduleInfo.name.toUpperCase()} has thrown error in command ${command.toUpperCase()}: ${err.message}`) }
    }); return;
  }
  if(parseInt(params[0]) > 200) {
    params[0] = 200;
  }
  let sides = parseInt(params[0])+1;

  var possible = [];
  for(var i=0; i < sides; i++){ if(i >= 1) { possible.push(i); } else { /*Nothing!*/ } }
  var y = possible[Math.floor(Math.random() * possible.length)];
  message.channel.sendMessage(`Eyy! You rolled a ${y}!`).catch((err) => {
    if($.config.debug) { print('error', `Module ${moduleInfo.name.toUpperCase()} has thrown error in command ${command.toUpperCase()}: ${err.message}`) }
  });
};

runFlipTheCoin = (message, params) => {
  let command = "flip";
  let flips = ['Heads', 'Heads', 'Heads', 'Heads', 'Tails', 'Tails', 'Tails', 'Tails'];

  var y = flips[Math.floor(Math.random() * flips.length)];
  message.channel.sendMessage(`${y}!`).catch((err) => {
    if($.config.debug) { print('error', `Module ${moduleInfo.name.toUpperCase()} has thrown error in command ${command.toUpperCase()}: ${err.message}`) }
  });
};

// YOU SHOULD NOT PUT PERSONAL CODE PAST THIS LINE!!!

// Register Command Objects
var newCommands = [
  { module: moduleInfo.module_exact, names: ['roll', 'rtd', 'random'], run: runRollTheDice },
  { module: moduleInfo.module_exact, names: ['flip', 'ftc', 'toss'], run: runFlipTheCoin }
];

// Registers Commands; Do not edit this or "newCommands" names.
this.register = (list) => {
  newCommands.forEach(command => { list.push(command); });
  return list;
};
