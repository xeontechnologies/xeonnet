this.init = () => {
  $.bot.on('message', (message) => {
    handleMessage(message);
  });
};

handleMessage = (message) => {
  let content = message.cleanContent;
  let command = content.split(' ')[0].slice($.config.prefix.length);
  let params = content.split(' ').slice(1);

  if(!content.startsWith($.config.prefix)) { return; }
  if(message.author.bot) { return; }
  if(message.author.id === $.bot.user.id) { return; }

  $.commands.forEach((cmd) => {
    if(cmd.names.indexOf(command) > -1) {
      cmd.run(message, params);
      if($.config.debug) { print('executed-command', `Guild <${message.guild.id}> executed | ${command + " " + params.join(' ')}`) }
    };
  });
}
